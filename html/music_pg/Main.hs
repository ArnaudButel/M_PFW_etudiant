{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)

data Name = Name T.Text deriving Show

instance FromRow Name where
    fromRow = Name <$> field 

main :: IO ()
main = do
    conn <- SQL.open "music.db"

    putStrLn "\n*** id et nom des artistes ***"
    -- TODO
    req1 <- SQL.query_ conn "SELECT * FROM artists" :: IO [(Int,T.Text)]
    mapM print req1

    putStrLn "\n*** id et nom de l'artiste 'Radiohead' ***"
    -- TODO
    req2 <- SQL.query conn 
        "SELECT * FROM artists WHERE name=?"
        (SQL.Only $ T.pack "Radiohead")
        :: IO [(Int, T.Text)]
    print req2

    putStrLn "\n*** tous les champs des titres dont le nom contient 'ust' et dont l'id > 1 ***"
    -- TODO
    req3 <- SQL.query conn 
        "SELECT * FROM titles WHERE name LIKE ? AND id>?"
        ("%ust%"::T.Text, 1::Int)
        :: IO [(Int, Int, T.Text)]
    print req3

    putStrLn "\n*** noms des titres (en utilisant le type Name) ***"
    -- TODO
    req4 <- SQL.query_ conn
        "SELECT name FROM titles"
        :: IO [(Name)]
    mapM print req4

    putStrLn "\n*** noms des titres (en utilisant une liste de T.Text) ***"
    -- TODO
    req5 <- SQL.query_ conn
        "SELECT name FROM titles"
        :: IO [([T.Text])]
    mapM print req5

    SQL.close conn

