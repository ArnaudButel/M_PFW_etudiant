{-# LANGUAGE OverloadedStrings #-}
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import qualified Data.Text.IO as TIO
import           Web.Scotty (scotty, get, html, param, rescue)
import           Lucid
import           System.Environment (getArgs, getProgName)

mainPage :: Html()
mainPage = do
    doctype_
    html_ $ do
      head_ $ do
        meta_ [charset_ "utf-8"]
      body_ $ do
        h1_ "The helloscotty project"
        a_ [href_ "/hello"] "go to hello page"


homePage :: Html()
homePage = do
    doctype_
    html_ $ do
      head_ $ do
        meta_ [charset_ "utf-8"]
      body_ $ do
        h1_ "Hello"
        a_ [href_ "/"] "go to home page"


main = scotty 3000 $ do
  -- http://localhost:3000/
  get "/" $ html . renderText $ mainPage

  -- http://localhost:3000/hello
  get "/hello" $ html . renderText $ homePage