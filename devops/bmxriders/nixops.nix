{
  network.description = "bmxriders";

  webserver = { config, pkgs, ... }: 
  let
    _bmxriders = import ./default.nix { inherit pkgs; };
  in
  {
    networking.firewall.allowedTCPPorts = [ 80 ];

    systemd.services.bmxriders = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" "postgresql.service" ];
      script = ''
        export DATABASE_URL="host=localhost port=5432 dbname=bmxridersdb user=bmxridersuser password='toto'"
        export PORT=80
        cd ${_bmxriders}
        ./bmxriders
      '';
    };

    services.postgresql = {
      enable = true;
      initialScript = "${_bmxriders}/bmxriders_full.sql";
      authentication = "local all all trust";
    };

    deployment = {
      targetEnv = "virtualbox";
      virtualbox = {
        memorySize = 512; 
        vcpu = 1; 
        headless = true;
      };
    };
  };
}

